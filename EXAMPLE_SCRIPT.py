#! /usr/bin/env python3

import csv
from freepbx_bulk_handler import Extension

# GLOBALS
INPUT_CSV_FILE = 'extensions_to_create.csv'
OUTPUT_CSV_FILE = 'extensions_to_import.csv'
EMAIL_DOMAIN = 'example.com'
VOICEMAIL_PASSWORD = '12345'


def write_csv_file(extensions) -> None:
    with open(OUTPUT_CSV_FILE, 'w', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=Extension.HEADER)
        writer.writeheader()
        writer.writerows(extension.to_dict for extension in extensions)


def main() -> None:
    # Create a "dumping place" where all the built Extension objects will be stored.
    extensions = []

    # Load input csv file
    fieldnames = ('Extension', 'First Name', 'Last Name')
    with open(INPUT_CSV_FILE, 'r') as f:
        raw_csv_input = csv.DictReader(f, quotechar='"', fieldnames=fieldnames)
        # Use the collected information to build each extension
        for row in raw_csv_input:
            # Skip the header row and blank rows, or rather, rows with no data in the first field.
            if row['Extension'] and row['Extension'] != 'Extension':
                extension = row['Extension']
                name = f"{row['First Name'].strip().capitalize()} {row['Last Name'].strip().capitalize()}"
                email = f"{row['First Name'].strip().lower()}.{row['Last Name'].strip().lower()}@{EMAIL_DOMAIN}"

                # Now that the pertinent information has been formatted, build the extension class
                data_vars = {'name': name,
                             'extension': extension,
                             'voicemail_vmpwd': VOICEMAIL_PASSWORD,
                             'voicemail_email': email}
                extensions.append(Extension(**data_vars))

    # Write finished extensions to a csv file.
    write_csv_file(extensions)


if __name__ == '__main__':
    main()
